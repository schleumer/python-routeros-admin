from flask import Flask
from helpers.template import *
from helpers.routeros import *

app = Flask(__name__)

apiros = RouterOS("10.1.1.1", "8728")
apiros.login("admin", "wes_130290")

@app.route('/')
def hello_world():
  r = apiros.talk(["/ip/dhcp-server/lease/print"])
  print(r)
  return t('index', name='ayyy')

if __name__ == '__main__':
  app.run(debug=True)